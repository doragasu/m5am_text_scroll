#include <stdio.h>
#include <esp_system.h>
#include <esp_spi_flash.h>
#include <driver/gpio.h>
#include <nvs_flash.h>
#include <scroller.h>
#include <driver/rmt.h>
#include <led_strip.h>
#include <simple_txt.h>

#include "util.h"
#include "font_5x5_64char.h"

#define M5AM_LED_PIN 27
#define M5AL_BUTTON_PIN 39
#define M5AL_BUTTON_MASK (((uint64_t)1)<<M5AL_BUTTON_PIN)


// LUT with byte order depending on rotation
static const uint32_t rot_lut[4][25] = {
	{
		3 *  0, 3 * 1,  3 *  2, 3 *  3, 3 *  4,
		3 *  5, 3 * 6,  3 *  7, 3 *  8, 3 *  9,
		3 * 10, 3 * 11, 3 * 12, 3 * 13, 3 * 14,
		3 * 15, 3 * 16, 3 * 17, 3 * 18, 3 * 19,
		3 * 20, 3 * 21, 3 * 22, 3 * 23, 3 * 24
	},
	{
		3 *  4, 3 *  9, 3 * 14, 3 * 19, 3 * 24,
		3 *  3, 3 *  8, 3 * 13, 3 * 18, 3 * 23,
		3 *  2, 3 *  7, 3 * 12, 3 * 17, 3 * 22,
		3 *  1, 3 *  6, 3 * 11, 3 * 16, 3 * 21,
		3 *  0, 3 *  5, 3 * 10, 3 * 15, 3 * 20
	},
	{
		3 * 24, 3 * 23, 3 * 22, 3 * 21, 3 * 20,
		3 * 19, 3 * 18, 3 * 17, 3 * 16, 3 * 15,
		3 * 14, 3 * 13, 3 * 12, 3 * 11, 3 * 10,
		3 *  9, 3 *  8, 3 *  7, 3 *  6, 3 *  5,
		3 *  4, 3 *  3, 3 *  2, 3 *  1, 3 *  0
	},
	{
		3 * 20, 3 * 15, 3 * 10, 3 *  5, 3 *  0,
		3 * 21, 3 * 16, 3 * 11, 3 *  6, 3 *  1,
		3 * 22, 3 * 17, 3 * 12, 3 *  7, 3 *  2,
		3 * 23, 3 * 18, 3 * 13, 3 *  8, 3 *  3,
		3 * 24, 3 * 19, 3 * 14, 3 *  9, 3 *  4
	}
};

static scr_handle scr;
static led_strip_t *led = NULL;

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
	uint32_t gpio_num = (uint32_t)arg;
	static uint32_t rot = 0;

	// Workaround: when WiFi is enabled, we get many interrupts
	// with level 1 that should not happen (approx. each 100 ms).
	// Filter them here.
	if (0 == gpio_get_level(gpio_num)) {
		rot++;
		scr_px_order_set(scr , rot_lut[rot & 3]);
	}
}

static void gpio_cfg(void)
{
	const gpio_config_t io_conf = {
		.pin_bit_mask = M5AL_BUTTON_MASK,
		.mode = GPIO_MODE_INPUT,
		.intr_type = GPIO_INTR_NEGEDGE,
		.pull_down_en = 0,
		.pull_up_en = 1
	};

	//configure GPIO with the given settings
	gpio_config(&io_conf);
	//install gpio isr service
	gpio_install_isr_service(0);
	gpio_isr_handler_add(M5AL_BUTTON_PIN, gpio_isr_handler, (void*)M5AL_BUTTON_PIN);
}

static void log_cpuinfo(void)
{
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	LOGI("ESP32 with %d CPU cores, WiFi%s%s, ", chip_info.cores,
			(chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
			(chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

	LOGI("silicon revision %d, ", chip_info.revision);

	LOGI("%dMB %s flash", (int)(spi_flash_get_chip_size() / (1024 * 1024)),
			(chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

}

static void scroll(const uint8_t *background, uint32_t cols)
{
	const struct scr_cfg cfg = {
		.map = {
			.width = cols,
			.height = 5
		},
		.screen = {
			.width = 5,
			.height = 5
		},
		.map_buf = background,
		.screen_buf =  led->buf_get(led)
	};

	scr = scr_init(&cfg);

	scr_draw(scr, 0, 0);
	led->refresh(led, 100);
	vTaskDelayMs(1000);
	int i = 0;
	while (true) {
		scr_draw(scr, i++, 0);
		led->refresh(led, 150);
		vTaskDelayMs(100);
	}
}

static void scr_test(void *arg)
{
	(void)arg;
	rmt_config_t rmt_cfg = RMT_DEFAULT_CONFIG_TX(M5AM_LED_PIN, RMT_CHANNEL_0);
	rmt_cfg.clk_div = 2;

	ESP_ERROR_CHECK(rmt_config(&rmt_cfg));
	ESP_ERROR_CHECK(rmt_driver_install(rmt_cfg.channel, 0, 0));

	led_strip_config_t led_cfg = {
		.max_leds = 25,
		.dev = (led_strip_dev_t)rmt_cfg.channel
	};

	led = led_strip_new_rmt_ws2812(&led_cfg);

	static const struct st_cfg font_cfg = {
		.font_data = (const uint8_t*)font_data,
		.font_spacing = font_char_width,
		.num_characters = FONT_CHARS,
		.font_data_width = FONT_WIDTH,
		.font_data_height = FONT_HEIGHT
	};

	static const struct st_color color = {
		.fg = {0, 0x1F, 0}
	};

	st_font_handle font = st_font_load(&font_cfg);

	static const char msg[] = "HELLO WORLD!!! ";
	const uint32_t cols = st_cols_get(font, msg);
	const uint32_t buf_width = cols * 3;
	uint8_t *font_buf = calloc(1, buf_width * 5);

	st_draw(font, msg, &color, font_buf, buf_width);

	scroll(font_buf, cols);
}

void app_main(void)
{
	log_cpuinfo();
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);

	gpio_cfg();

	xTaskCreate(scr_test, "test", 2048, NULL, tskIDLE_PRIORITY + 1, NULL);
}

