#ifndef _FONT_5X5_64CHAR_H_
#define _FONT_5X5_64CHAR_H_

#include <stdint.h>

#define FONT_WIDTH 296
#define FONT_HEIGHT 5
#define FONT_CHARS 64

// 185 bytes
#define FONT_LEN (FONT_HEIGHT * FONT_WIDTH / 8)

extern const uint8_t font_char_width[FONT_CHARS];
extern const uint8_t font_data[FONT_HEIGHT][FONT_WIDTH / 8];

#endif /*_FONT_5X5_64CHAR_H_*/
