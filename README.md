# Introduction

This project uses the [simple\_txt](https://gitlab.com/doragasu/simple_txt) and [scroller](https://gitlab.com/doragasu/scroller) submodules to draw a scrolling text on the 5x5 LED matrix in the M5Atom Matrix devices.  For this purpose, I have created a 5-row, 64 character font sample. You can view it in the `resource` subdirectory.

The code is prepared to be built and flashed with Espressif [esp-idf](https://github.com/espressif/esp-idf) SDK. It also uses a slightly modified version of the `led_strip` component provided in the `esp-idf` SDK examples.

# License

This project is provided with NO WARRANTY under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.en.html).

